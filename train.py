from utils import (
    WordBank, Calculator, Word,
)
class Train:
    def __init__(self, dao, file_name='labels'):
        self.file_name = file_name
        self.spam_dao = dao

    def obtain_spam_ham_probability(self):
        self.spam_dao.drop_statistics()
        print("Obtaining spam, ham probabilities...")

        counts = self.spam_dao.get_total_counts()
        self.total_ham_count = counts[0] 
        self.total_spam_count = counts[1] 
        self.total_count = counts[0] + counts[1]

        self.ham_probability = Calculator.divide(
            self.total_ham_count, self.total_count
        )
        self.spam_probability = Calculator.divide(
            self.total_spam_count, self.total_count
        )

        self.spam_dao.record_statistics(
            'Observed Ham Probability', 
            self.ham_probability
        )

        self.spam_dao.record_statistics(
            'Observed Spam Probability', 
            self.spam_probability
        )

        print("Obtained")

    def obtain_likelihood(self, l=1):
        self.spam_dao.drop_likelihood()
        print("Obtaining likelihood........")

        spam_occurences = self.spam_dao.get_occurences_in_classification('spam')
        ham_occurences = self.spam_dao.get_occurences_in_classification('ham')

        likelihood_bank = {}

        for spam_occurence in spam_occurences:
            word = spam_occurence[0]
            value = spam_occurence[1]

            if word not in likelihood_bank:
                likelihood_bank[word] = Word()
            
            likelihood_bank[word].add_spam_count(value)


        for ham_occurence in ham_occurences:
            word = ham_occurence[0]
            value = ham_occurence[1]

            if word not in likelihood_bank:
                likelihood_bank[word] = Word()
            
            likelihood_bank[word].add_ham_count(value)

        vocabulary_size = len(likelihood_bank)

        for word in likelihood_bank:
            cur_spam_count = likelihood_bank[word].get_spam_count()

            cur_spam_prob = Calculator.lambda_smoothing(
                cur_spam_count,
                self.total_spam_count,
                vocabulary_size,
                l
            )
            cur_ham_count = likelihood_bank[word].get_ham_count()

            cur_ham_prob = Calculator.lambda_smoothing(
                cur_ham_count,
                self.total_ham_count,
                vocabulary_size,
                l
            )

            mi_cur_ham_prob = Calculator.lambda_smoothing(
                cur_ham_count,
                self.total_ham_count,
                200,
                l
            )

            mi_cur_spam_prob = Calculator.lambda_smoothing(
                cur_spam_count,
                self.total_spam_count,
                200,
                l
            )
           
            mutual_information = Calculator.mutual_information(
                cur_spam_count,
                cur_ham_count,
                self.total_spam_count,
                self.total_ham_count,
                self.spam_probability,
                self.ham_probability
            )

            self.spam_dao.add_likelihood(
                word, cur_ham_prob, cur_spam_prob, mutual_information, mi_cur_ham_prob, mi_cur_spam_prob
            )

        print("Obtained!")

    def export_results(self):
        print("Exporting...")
        likelihoods = self.spam_dao.get_likelihoods()
        file_output = open('train_results.txt', 'w')

        file_output.write(
            "Overall Spam Probability: "
            + str(self.spam_probability)
            + "\tOverall Ham Probability: "
            + str(self.ham_probability)
            +'\n'
        )
         
        for item in likelihoods:
            word = item
            prob_ham = likelihoods[item].ham_prob
            prob_spam = likelihoods[item].spam_prob
            file_output.write(
                "Word:" 
                + word
                + "\tHam prob:"
                + str(prob_ham)
                + "\tSpam Prob: " 
                + str(prob_spam)
                + '\n'
            )
        file_output.close()

        print("Exported! File name 'train_results.txt'")

