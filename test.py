from __future__ import print_function
from utils import WordBank, Parser, SpamDAO, Calculator, File

class Test:
    def __init__(self, dao,  file_name='labels'):
        self.file_name = file_name
        self.spam_dao = dao
        self.file_output = open('test_results.txt', 'w')

    def get_all_files(self): 
        all_files = self.spam_dao.get_all_files(data_type='test')

        self.all_files = [
            File(file_id=item[0], name=item[1], classification=item[2]) 
            for item in all_files
        ] 

    def write_result(self, file_object):
        self.file_output.write(
            "File name: " + file_object.get_name()
            + "\tClassification: " + file_object.get_classification()
            + "\tTest result: " + file_object.get_test_result() + '\n'
        )

    def classify_file(self, file_name):
        statistics = self.spam_dao.get_statistics()
        spam_class_prob = statistics[1][1]
        ham_class_prob = statistics[0][1]
        likelihoods = self.spam_dao.get_likelihoods()
        
        file_input = open(file_name, 'r')
        contents = file_input.read()
       
        word_list = set(Parser.parse_content(contents))
        
        result = Calculator.naive_bayes(
                word_list, likelihoods, spam_class_prob, ham_class_prob
        )
        return result
         
    def batch_test(self, mi=0):
        self.get_all_files()
        l = float(open('lambda_file', 'r').read())
        statistics = self.spam_dao.get_statistics()
        spam_class_prob = statistics[1][1]
        ham_class_prob = statistics[0][1]
        likelihoods = self.spam_dao.get_likelihoods() if not mi \
            else self.spam_dao.get_likelihoods_mi()

        word_lists = self.spam_dao.get_all_words('test')

        for current_file in self.all_files:
            word_list = set(word_lists[current_file.get_file_id()])

            current_file.set_test_result(
                Calculator.naive_bayes(
                    word_list, likelihoods, spam_class_prob, ham_class_prob, l
                )
            )

            print("Current File:", current_file.name )
            self.write_result(current_file)
            self.spam_dao.update_file(current_file)
        
        self.spam_dao.db.commit()

    def genererate_precision_recall(self):
        fn = self.spam_dao.get_test_results('spam', 'ham')
        fp = self.spam_dao.get_test_results('ham', 'spam')
        tn = self.spam_dao.get_test_results('ham', 'ham')
        tp = self.spam_dao.get_test_results('spam', 'spam')

        precision = Calculator.divide(tp, tp + fp)
        recall = Calculator.divide(tp, tp + fn)

        self.spam_dao.record_statistics('precision', precision)
        self.spam_dao.record_statistics('recall', recall)

        statistics_file = open('statistics', 'w')
        print("\n\n\nPrecision: ", precision, "Recall:", recall)
        statistics_file.write("Precision: " + str(precision) + " Recall: " + str(recall))
        statistics_file.close()
