# Spam Filter using Naive Bayes Classifier with Lambda Smoothing

This project is for the CS 280 programming assignment 1.

We are to create a spam filter that implements the Naive Bayes Classifier.

## Project Technologies

This program was written in Python. The database was implemented using sqlite3

## Instructions

To run the program, enter in your unix terminal:

`./main.py`

or 'python main.py'

The program will then create the database if it is still not yet created.

### Menu Screen
To train, input 1, it will train the data set and will prompt the user for a lambda.

Then after training, you will have the option to output the training results, which are the results of the training. The output file contains the prior probability of both spam and ham classes. It also contains all the words in the dictionary with their coresponding ham and spam probabilities.

Take not that the mutual information probability dictionary is already computed in the training to save time.

To test, input 2. 

It will prompt the user whether he/she wants to test using the mutual information dictoinary or the regular dictionary. The mutual information dictionary is fater than the regular dictionary. Also, you cannot test unless you have trained.

Should you chose to train using mutual information, the program will automatically outut the top 200 most informative words calculated by mutual information. The output is contained in the 'top_200.txt'

The precision and recall is also saved in a file named 'statistics'

To classify a file, input 3.
It will prompt you to enter a file name which you want to test.
