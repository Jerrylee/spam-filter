from __future__ import print_function
from operator import itemgetter
import math
import os
import json
import ast
import re
import sqlite3
import time


class Likelihood:
    def __init__(self, word='', spam_prob=0, ham_prob=0):
        self.set_ham_prob(ham_prob)
        self.set_spam_prob(spam_prob)
        self.set_word(word)

    def set_ham_prob(self, ham_prob):
        self.ham_prob = ham_prob

    def set_spam_prob(self, spam_prob):
        self.spam_prob = spam_prob

    def set_word(self, word):
        self.word = word

    def get_ham_prob(self):
        return self.ham_prob

    def get_spam_prob(self):
        return self.spam_prob

    def get_word(self):
        return self.word

class Word:
    def __init__(self, new_name=''):
        self.ham_count = 0
        self.spam_count = 0
        self.set_name(new_name)

    def add_ham_count(self, ham_count):
        self.ham_count = self.ham_count  + ham_count

    def add_spam_count(self, spam_count):
        self.spam_count = self.spam_count  + spam_count

    def set_name(self, new_name):
        self.name = new_name
    
    def get_spam_count(self):
        return self.spam_count

    def get_ham_count(self):
        return self.ham_count

class File:
    def __init__(self, file_id=0, name='', classification='', test_result=''):
        self.set_name(name)
        self.set_classification(classification)
        self.set_file_id(file_id)
        self.set_test_result(test_result)

    def set_file_id(self, file_id):
        self.set_file_id = file_id

    def set_name(self, name):
        self.name = name
    
    def set_classification(self, classification):
        self.classification = classification

    def set_test_result(self, test_result):
        self.test_result = test_result

    def get_classification(self):
        return self.classification

    def get_name(self):
        return self.name

    def get_file_id(self):
        return self.set_file_id

    def get_test_result(self):
        return self.test_result

    def __str__(self):
        return self.name + ' ' + self.classification

class WordBank:
    def __init__(self):
        self.file_name = ""
        self.classification = ""
        self.contents = set() 
        self.data_type = ''

    def set_contents(self, contents):
        self.contents = contents
    
    def set_file_name(self, file_name):
        self.file_name = file_name

    def set_classification(self, classification):
        self.classification = classification

    def set_data_type(self, data_type):
        self.data_type = data_type
    
    def __str__(self):
        return self.file_name + ' ' + self.classification + ' ' + self.contents

class Parser:
    @staticmethod
    def parse_content(contents, word_bank=None):
        contents = set(re.split(';|,|; |, | |\n|\n |\t', contents))

        alpha_content = set([
            content.lower() if content.isalpha() else '' for content in contents
        ])

        if word_bank:
            word_bank.set_contents(alpha_content)
        else:
            return alpha_content

    @staticmethod
    def parse_classification(classfication_string, word_bank):
        classfication_array = classfication_string.split() 
        word_bank.set_file_name(classfication_array[1])
        word_bank.set_classification(classfication_array[0])
        current_directory = int(classfication_string.split('/')[1])
        data_type = 'test' if current_directory > 70 else 'train'
        word_bank.set_data_type(data_type)

class SpamDAO:
    def __init__(self):
        self.db = None

        if os.path.isfile('spam_filter.db'):
            self.db = sqlite3.connect('spam_filter.db')
            print("Connecting to Database")
            time.sleep(1)
            print('Connected Successfully')
        else:
            print('Creating Database...')
            time.sleep(1)
            db = open('spam_filter.db', 'w')
            db.close()
            self.db = sqlite3.connect('spam_filter.db')
            self.create_tables()
            self.read_master_file()
            
            print('Successfully created. Connecting to database...')
            time.sleep(1)
            print('Connected Successfully')

    def read_master_file(self, file_name='labels'):
        print("Importing file...")

        with open(file_name, 'r') as file_input:
            for line_number, current_line in enumerate(file_input):
                print("Current file:", current_line.split()[1], end='\r')

                new_word_bank = WordBank()
                Parser.parse_classification(current_line, new_word_bank)
                contents = open(new_word_bank.file_name, 'r')
                contents = contents.read()
                Parser.parse_content(contents, new_word_bank)
                self.import_word_bank(new_word_bank, line_number)
        print("File imported!")
        self.db.commit()
        
    def create_tables(self, database_filename='database.sql'):
        database_file = open(database_filename, 'r') 
        self.db.executescript(database_file.read())
        database_file.close()
        self.db.commit()

    def update_file(self, f):
        self.db.execute(
            """
                UPDATE file SET test_result = ? WHERE id = ?;
            """
            , (f.get_test_result(), f.get_file_id())
        )
        

    def import_word_bank(self, word_bank, file_id):
        self.db.execute(
                """
                   INSERT INTO file(name, classification, data_type) VALUES (?, ?, ?);
                """
                , (word_bank.file_name, word_bank.classification, word_bank.data_type)
        )

        for key in word_bank.contents:
            self.db.execute(
                """
                    INSERT INTO word(name, file_id) VALUES (?, ?);
                """
                , (key, file_id)
            )

    def get_total_counts(self):
        query = self.db.execute(
            """
                SELECT classification, COUNT(id)
                FROM file
                WHERE data_type = 'train'
                GROUP BY classification;
            """
        )

        objects = query.fetchall()
        objects = sorted(objects, key=itemgetter(0))

        return [item[1] for item in objects]
    
    def record_statistics(self, name, value):
        self.db.execute(
            """
                INSERT INTO statistics(label, value) VALUES (?, ?);
            """
            ,(name, value)
        )

        self.db.commit()
    
    def get_statistics(self):
        query = self.db.execute(
            """
                SELECT label, value FROM statistics;
            """
        )

        objects = query.fetchall()

        return objects

    def get_all_words_in_file(self, file_id):
        '''
        query = self.db.execute(
            """
                SELECT word.name
                FROM file 
                JOIN word ON (word.file_id = file.id) 
                WHERE file.name=? 
                ORDER BY word.name;
            """
            , (file_name,)
        )
        '''
        query = self.db.execute(
            """
                SELECT name
                FROM word
                WHERE file_id=? 
                ORDER BY name;
            """
            , (file_id,)
        )
        objects = query.fetchall()

        result = [
            item[0] for item in objects
        ]
        
        return result

    def get_all_files(self, parameter='', data_type='train'):

        query = (
            self.db.execute(
                """
                    SELECT name
                    FROM file
                    WHERE classification = ? AND data_type = 'train';
                """
                , (parameter,)
            ) if parameter else  
            self.db.execute(
                """
                    SELECT id, name, classification
                    FROM file
                    WHERE data_type = ?
                    ORDER BY name;
                """
                , (data_type,)
            )
        )

        objects = query.fetchall()

        return objects

    def get_all_words(self, classification='test'):
        query = self.db.execute(
            """
                SELECT word.name, file.id
                FROM file
                LEFT JOIN word ON (file.id = word.file_id)
                WHERE data_type = ?
                ORDER BY file_id, word.name;
            """
            , (classification,)
        )

        objects = query.fetchall()
        
        result = {}
        for item in objects:
            key = item[1]
            value = item[0]

            if key not in result:
                result[key] = []

            result[key].append(value)

        return result

    def add_likelihood(self, word, prob_ham, prob_spam, mi, mi_prob_ham, mi_prob_spam):
        self.db.execute(
            """
                INSERT INTO likelihood(
                    word, probability_ham, probability_spam, mutual_information,
                    mi_probability_ham, mi_probability_spam
                )
                VALUES (?, ?, ?, ?, ?, ?);
            """
            , (word, prob_ham, prob_spam, mi, mi_prob_ham, mi_prob_spam)
        )
        

    def drop_likelihood(self):
        self.db.execute("DELETE FROM likelihood;")
        self.db.commit()

    def drop_statistics(self):
        self.db.execute("DELETE FROM statistics;")
        self.db.commit()

    def get_occurence(self, word, classification):
        query = self.db.execute(
            """
                SELECT count(word.name)
                FROM word
                JOIN file ON (word.file_id = file.id)
                WHERE classification = ? AND word.name = ? AND data_type = 'train';
            """
            , (classification, word)
        )

        objects = query.fetchone()

        return objects[0]
    
    def get_occurences_in_classification(self, classification):
        query = self.db.execute(
            """
                SELECT word.name, count(word.name)
                FROM word
                JOIN file ON (word.file_id = file.id)
                WHERE classification = ? AND data_type = 'train'
                GROUP BY word.name
            """
            , (classification,)
        )

        objects = query.fetchall()
        
        return objects
    
    def get_likelihoods(self):
        query = self.db.execute(
            """
                SELECT word, probability_ham, probability_spam
                FROM likelihood
                ORDER BY word;

            """
        )

        objects = query.fetchall()

        likelihoods = {
            item[0]: Likelihood(word=item[0], ham_prob=item[1], spam_prob=item[2])
            for item in objects
        } 

        return likelihoods

    def get_likelihoods_mi(self):
        query = self.db.execute(
            """
                SELECT *
                FROM (
                    SELECT word, mi_probability_ham, mi_probability_spam
                    FROM likelihood
                    ORDER BY mutual_information DESC
                    LIMIT 200
                ) ORDER BY word;

            """
        )

        objects = query.fetchall()

        likelihoods = {
            item[0]: Likelihood(word=item[0], ham_prob=item[1], spam_prob=item[2])
            for item in objects
        } 

        return likelihoods

    def get_test_results(self, expected, actual):
        query = self.db.execute(
            """
                SELECT count(*)
                FROM file
                WHERE classification = ? AND test_result = ?;
            """
            , (expected, actual)
        )

        item = query.fetchone()

        result = item[0]

        return result


class Calculator:
    @staticmethod
    def divide(num, den):
        exponent = 0
        if num < 0:
            exponent = exponent + 1
        if den < 0:
            exponent = exponent + 1

        sign = (-1)**exponent

        result = sign*math.exp(math.log(abs(num)) - math.log(abs(den))) if num else 0

        return result

    @staticmethod
    def multiply(x, y):

        exponent = 0
        if x < 0:
            exponent = exponent + 1
        if y < 0:
            exponent = exponent + 1

        sign = (-1)**exponent
        result = sign * math.exp(math.log(abs(x)) + math.log(abs(y))) if x and y else 0

        return result

    @staticmethod
    def lambda_smoothing(num, den, V, l=1):
        result = Calculator.divide(num + l, den + l*V)

        return result

    @staticmethod
    def naive_bayes(word_list, likelihoods, spam_class_prob, ham_class_prob, l):
        spam_product = 1
        ham_product = 1

        likelihood_set = set(likelihoods)
        vocab_size = len(likelihoods)
        opposite_set = likelihood_set.difference(word_list)
        real_set = likelihood_set.intersection(word_list)

        for key in opposite_set:
            likelihood = likelihoods[key]
            spam_factor = 1 - likelihood.get_spam_prob()
            if not spam_factor:
                spam_factor = Calculator.divide(l, l*vocab_size)
            ham_factor = 1 - likelihood.get_ham_prob()
            if not ham_factor:
                ham_factor = Calculator.divide(l, l*vocab_size)
            spam_product = Calculator.multiply(spam_product, spam_factor)
            ham_product = Calculator.multiply(ham_product, ham_factor)

        for key in real_set:
            likelihood = likelihoods[key]
            spam_factor = likelihood.get_spam_prob()
            ham_factor = likelihood.get_ham_prob()
            spam_product = Calculator.multiply(spam_product, spam_factor)
            ham_product = Calculator.multiply(ham_product, ham_factor)
        '''
        for key in likelihoods:
            likelihood = likelihoods[key]
            ham_factor = likelihood.get_ham_prob()
            spam_factor = likelihood.get_spam_prob()

            if not binary_search(word_list, key):
                ham_factor = 1 - ham_factor 
                spam_factor = 1 - spam_factor 
            spam_product = Calculator.multiply(spam_product, spam_factor)
            ham_product = Calculator.multiply(ham_product, ham_factor)

        '''
        denominator = spam_product + ham_product

        spam_result = Calculator.divide(spam_product, denominator)
        ham_result = Calculator.divide(ham_product, denominator)

        return Calculator.arg_max(spam_result, ham_result) 

    @staticmethod
    def arg_max(spam,ham):
        return 'ham' if spam <= ham else 'spam'

    @staticmethod
    def mutual_information(
        spam_count,
        ham_count,
        t_spam_count,
        t_ham_count,
        prior_spam_prob,
        prior_ham_prob
    ):
        word_total_count = spam_count + ham_count
        t_count = t_spam_count + t_ham_count
        
        """
        print("Total word count:", word_total_count,
              "Total count:", t_count)
        """
        P_spam = prior_spam_prob
        P_ham = prior_ham_prob
        P_word = Calculator.divide(word_total_count, t_count)
        P_word_not = 1 - P_word
        P_word_spam = Calculator.divide(spam_count, t_spam_count)
        P_word_not_spam = 1 - P_word_spam
        P_word_ham = Calculator.divide(ham_count, t_ham_count)
        P_word_not_ham = 1 - P_word_ham

        """
        print("P(word_ham):", P_word_ham,
              "P(word_not_ham):", P_word_not_ham,
              "P(word_spam):", P_word_spam,
              "P(word_not_spam):", P_word_not_spam,
              "P(word):", P_word,
              "P(word_not):", P_word_not)
        """
        inner_fraction = Calculator.divide(
            P_word_spam,
            Calculator.multiply(
                P_word,
                P_spam
            )
        )

        first = Calculator.multiply(
            P_word_spam,
            math.log(inner_fraction)
        ) if inner_fraction else 0

        inner_fraction = Calculator.divide(
            P_word_ham,
            Calculator.multiply(
                P_word,
                P_ham
            )
        )
        second = Calculator.multiply(
            P_word_ham,
            math.log(inner_fraction)
        ) if inner_fraction else 0

        inner_fraction = Calculator.divide(
            P_word_not_spam,
            Calculator.multiply(
                P_word_not,
                P_spam
            )
        )

        third = Calculator.multiply(
            P_word_not_spam,
            math.log(inner_fraction)
        ) if inner_fraction else 0

        inner_fraction = Calculator.divide(
            P_word_not_ham,
            Calculator.multiply(
                P_word_not,
                P_ham
            )
        )
        fourth = Calculator.multiply(
            P_word_not_ham,
            math.log(inner_fraction)
        ) if inner_fraction else 0

        #print(first,second,third,fourth)
        mi = first + second + third + fourth

        return mi
        

def binary_search(alist, item):
    first = 0
    last = len(alist) - 1
    found = False

    while first <= last and not found:
        midpoint = (first + last)//2
        if alist[midpoint] == item:
            found = True
        else:
            if item < alist[midpoint]:
                last = midpoint-1
            else:
                first = midpoint+1

    return found
