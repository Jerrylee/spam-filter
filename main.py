#!/usr/bin/python
from utils import SpamDAO, Parser
from train import Train
from test import Test
import subprocess as sp
import time


print "Welcome to Spam Filter using Naive-Bayes Classifier"
dao = SpamDAO()
trainer = Train(dao)
tester = Test(dao)

while True:
    try:
        choice = input("What would you like to do? [1] Train [2] Test [3] Classify a file\n:")
    except:
        print "Please enter an integer."

    if choice == 1:
        trainer.obtain_spam_ham_probability()
        l = input("Enter lambda: ")
        open('lambda_file', 'w').write(str(l))
        trainer.obtain_likelihood(l)
        dao.db.commit()

        export_choice = input("Do you want to export? [1] Yes [<anything>] No: ")
        if export_choice == 1:
            trainer.export_results()

    elif choice == 2:
        mi_choice = input(
            """Do you want to test using mutual information?: 
            [1] Yes  [anything] otherwise."""
        )
        mi_choice = mi_choice if mi_choice == 1 else 0
        tester.batch_test(mi_choice)
        tester.genererate_precision_recall()
        if mi_choice:
            mi = dao.get_likelihoods_mi()
            with open('top_200.txt', 'w') as mi_file:
                for item in mi:
                    mi_file.write(item+'\n')
                
        dao.db.commit()

    elif choice == 3:
        file_name = raw_input("Enter the file name of the text you wish to classify.")
        classification = tester.classify_file(file_name)
        print "Classification: ", classification

        time.sleep(2)

    else:
        print "Good bye."
        break

    time.sleep(.75)
    tmp = sp.call('clear',shell=True)
