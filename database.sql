DROP TABLE IF EXISTS 'file';
CREATE TABLE file(
    id INTEGER NOT NULL PRIMARY KEY,
    classification VARCHAR(5) NOT NULL,
    name VARCHAR(30) NOT NULL,
    data_type VARCHAR(30) NOT NULL,
    test_result VARCHAR(30) NOT NULL DEFAULT 'ham'
);

DROP TABLE IF EXISTS 'word';
CREATE TABLE word(
    id INTEGER NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    file_id INTEGER NOT NULL,

    FOREIGN KEY(file_id) REFERENCES file(id)
);

DROP TABLE IF EXISTS 'likelihood';
CREATE TABLE likelihood(
    id INTEGER NOT NULL PRIMARY KEY,
    word TEXT NOT NULL,
    probability_ham REAL NOT NULL,
    probability_spam REAL NOT NULL,
    mi_probability_ham REAL NOT NULL,
    mi_probability_spam REAL NOT NULL,
    mutual_information REAL NOT NULL
);

DROP TABLE IF EXISTS 'statistics';
CREATE TABLE statistics(
    id INTEGER NOT NULL PRIMARY KEY,
    label VARCHAR(255) NOT NULL,
    value REAL NOT NULL
);
